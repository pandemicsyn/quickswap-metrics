from uniswap import Uniswap

address = None  # or None if you're not going to make transactions
private_key = None  # or None if you're not going to make transactions
version = 2  # specify which version of Uniswap to use
provider = "https://mainnet.infura.io/v3/48fb6dce98084af8b8e371f0e299b526"  # can also be set through the environment variable `PROVIDER`

uniswap = Uniswap(
    address=address, private_key=private_key, version=version, provider=provider
)

eth = "0x0000000000000000000000000000000000000000"
bat = "0x0D8775F648430679A709E98d2b0Cb6250d2887EF"
dai = "0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359"

print(uniswap.get_price_input(eth, dai, 10 ** 18))
